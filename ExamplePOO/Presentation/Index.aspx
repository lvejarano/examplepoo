﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Presentation.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Ingresar el Color:"></asp:Label>
            <asp:TextBox ID="TBColor" runat="server"></asp:TextBox><br />
             <asp:Label ID="Label2" runat="server" Text="Ingresar el Cilindraje:"></asp:Label>
            <asp:TextBox ID="TBCilindraje" runat="server"></asp:TextBox><br />
             <asp:Label ID="Label3" runat="server" Text="Ingresar el Modelo:"></asp:Label>
            <asp:TextBox ID="TBModelo" runat="server"></asp:TextBox><br />
            <asp:Label ID="LblMsj" runat="server" Text=""></asp:Label><br />
            <asp:Button ID="BtnEnviar" runat="server" Text="Enviar" OnClick="BtnEnviar_Click" />
        </div>
    </form>
</body>
</html>
