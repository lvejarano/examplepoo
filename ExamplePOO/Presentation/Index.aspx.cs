﻿using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Index : System.Web.UI.Page
    {
        Car objCar = new Car();
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnEnviar_Click(object sender, EventArgs e)
        {
            objCar.Color = TBColor.Text; //Se asigna (set) el valor ingresado desde la caja de texto
            objCar.EngineCylinderVolumen = Convert.ToInt32(TBCilindraje.Text);//Se asigna (set) el valor ingresado desde la caja de texto
            objCar.Model = Convert.ToInt32(TBModelo.Text);//Se asigna (set) el valor ingresado desde la caja de texto
            LblMsj.Text = objCar.stop(); //Se obtiene (get) los valor ingresados desde las cajas de texto
        }
    }
}